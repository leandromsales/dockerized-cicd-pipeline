# Dockerized Pipelines

* Just include this project as documented at https://docs.gitlab.com/ee/ci/yaml/includes.html#single-string-or-array-of-multiple-values

```yaml
# https://docs.gitlab.com/ee/ci/yaml/includes.html#single-string-or-array-of-multiple-values
# Reusing the pipeline

include:
  remote: 'https://gitlab.com/marcellodesales/dockerized-cicd-pipeline/raw/master/.gitlab-cicd-dockerized-pipeline.yaml'
```

# Requirements

This assumes your repo has the following:

* `Dockerfile`: Containing a build of a project
* `docker-compose.yaml`: Drives the build, generates a local image name

# Stages 

# Setup 

* Verifies the docker engine version, etc

# Lint

* Verifies the `Dockerfile` 
* Verifies the `docker-compose.yaml`

# Docker-build

* Build Docker Image: It executes `docker-compose build`
* Tags the build image as the Container Registry image
  * PROJECT-CONTAINER-REPO:`SHA`: The full SHA to identify the given commit
  * PROJECT-CONTAINER-REPO:`SHORT_SHA-BRANCH`: The Short SHA and the branch to use in GitOps
* Pushes all tags to the container registry
